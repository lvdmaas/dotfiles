fpath+=~/.zfunc

source $HOME/.antigen/antigen.zsh

antigen use oh-my-zsh

antigen bundle git
antigen bundle pip
antigen bundle command-not-found
antigen bundle debian
antigen bundle common-aliases
antigen bundle docker
antigen bundle python

antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-autosuggestions

# Load the theme
antigen bundle tonyseek/oh-my-zsh-virtualenv-prompt
antigen theme tonyseek/oh-my-zsh-seeker-theme seeker

# Tell antigen that you're done
antigen apply

# Apply colors
export NO_AT_BRIDGE=1
(wpg -t &)

alias trs=trash
alias config='/usr/bin/git --git-dir=$HOME/.config/vcs/ --work-tree=$HOME'

export EDITOR=vim
export PATH=/opt/android-studio/bin:$HOME/.cargo/bin:/usr/local/texlive/2018/bin/x86_64-linux:$HOME/.poetry/bin:$PATH

[ -e $HOME/.zsh/notifyosd.zsh ] && . $HOME/.zsh/notifyosd.zsh
