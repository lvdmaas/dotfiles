#!/bin/bash

# switch bewteen the available ports

sink=`pacmd list-sinks |awk '/\* index:/{print $3}'`
default='analog-output-speaker'
other='analog-output-headphones'

port=`pactl list sinks | grep "Active Port" | sed 's/\s*Active Port:\s*//g'`
if [ ${port} = ${default} ]
then
	pacmd set-sink-port ${sink} ${other}
else
	pacmd set-sink-port ${sink} ${default}
fi
