set nocompatible              " be iMproved, required
filetype off                  " required
syntax on
set autoindent
set number
set shiftwidth=4
set tabstop=4
set expandtab
set encoding=utf-8
set shell=/bin/bash

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

Plugin 'rust-lang/rust.vim'
Plugin 'vim-syntastic/syntastic'
Plugin 'Valloric/YouCompleteMe'

call vundle#end()            " required
filetype plugin indent on    " required

let g:ycm_rust_src_path="/home/luko/prj/builds/rust-master/src"

au BufRead,BufNewFile *.rs set filetype=rust
